# babel-and-node #

* [babel usage](https://babeljs.io/docs/en/usage)

npm init
#  transforming new syntax and polyfilling missing features.
npm install --save-dev @babel/core @babel/cli @babel/preset-env
npm install --save @babel/polyfill
# create babel.config.js to identify presets
# transpile js in code directory and output js to dist directory
npx babel code --out-dir dist
